import store from '../store';
const NotifierPlugin = {
  install: Vue => {
    Vue.showNotification = ({ content = '', color = '' }) => {
      store.commit('snackbar/showMessage', { content, color })
    }
  }
}

export default NotifierPlugin;


