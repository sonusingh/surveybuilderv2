import Axios from "axios";

class QuestionDataService {

    saveQuestion(data){
       return axios.post('/api/question',data).then(response=>{
            return response.data;
        });
    }

}

export default new QuestionDataService;