
class RoleDataService {
    getAll() {
      return axios.get("/role");
    }
  
    get(id) {
      return axios.get(`/role/${id}`);
    }
  
    create(data) {
      return axios.post("/role", data);
    }
  
    update(id, data) {
      return axios.put(`/role/${id}`, data);
    }
  
    delete(id) {
      return axios.delete(`/role/${id}`);
    }
  
    deleteAll() {
      return axios.delete(`/roles`);
    }
  
    findByTitle(name) {
      return axios.get(`/role?name=${name}`);
    }
  }
  
  export default new RoleDataService();
  