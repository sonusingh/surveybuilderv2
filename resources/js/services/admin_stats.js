class AdminStatsDataService 

{

    getStats(){

        return axios.get('/api/admin_get_stats').then(response=>{
            
            return response.data;

        })
    }

}

export default new AdminStatsDataService;