class LoginService{
    login(data){
        return axios.post('/login',data)
    }
    logout(){
       return  axios.post('/logout');
    }
    getSanctumCookies(){
        return axios.get('/sanctum/csrf-cookie');
    }

    getUser(){
        return axios.get('/user');
    }
}

export default new LoginService;