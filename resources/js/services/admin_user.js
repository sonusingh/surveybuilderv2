
class AdminUserDataService {
    getAll() {
      return axios.get("/admin_user");
    }
  
    get(id) {
      return axios.get(`/admin_user/${id}`);
    }
  
    create(data) {
      return axios.post("/admin_user", data);
    }
  
    update(id, data) {
      return axios.put(`/admin_user/${id}`, data);
    }
  
    delete(id) {
      return axios.delete(`/admin_user/${id}`);
    }
  
    deleteAll() {
      return axios.delete(`/admin_user`);
    }
  
    findByTitle(name) {
      return axios.get(`/admin_user?name=${name}`);
    }
  }
  
  export default new AdminUserDataService();
  