class SurveyDataService{

    getSurvey(page){

        return  axios.get('/api/survey?page='+page).then(response=>{
            
            return response.data.surveys;

        })

    }

    createSurvey(formData){

        return axios.post('/api/survey',formData).then(response => {

            return response;

        })

    }

    updateSurvey(id,formData){

        return axios.put('/api/survey/'+id,formData).then(response=>{

            return response;

        });

    }

    deleteSurvey(id){

        return axios.delete('/api/survey/'+id).then(response=>{

            return response;

        });

    }
}

export default new SurveyDataService;