class UploadFileService{
    
    upload(data){
        let config = { headers: { 'Content-Type': 'multipart/form-data' } };
        return axios.post('/api/upload',data,config).then(response=>{
               return response.data;
        });
    }
    
}
export default new UploadFileService;