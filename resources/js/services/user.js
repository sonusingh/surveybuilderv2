
class UserDataService {
    getAll() {
      return axios.get("/user");
    }
  
    get(id) {
      return axios.get(`/user/${id}`);
    }
  
    create(data) {
      return axios.post("/user", data);
    }
  
    update(id, data) {
      return axios.put(`/user/${id}`, data);
    }
  
    delete(id) {
      return axios.delete(`/user/${id}`);
    }
  
    deleteAll() {
      return axios.delete(`/users`);
    }
  
    findByTitle(name) {
      return axios.get(`/user?name=${name}`);
    }
  }
  
  export default new UserDataService();
  