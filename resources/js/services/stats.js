import Axios from "axios";

class StatsDataService 

{

    getStats(){

        return axios.get('/api/get_stats').then(response=>{
            
            return response.data;

        })
    }

}

export default new StatsDataService;