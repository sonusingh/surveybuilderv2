class CompanyDataService{

    getCompany(page){

        return  axios.get('/api/company?page='+page).then(response=>{

            return response.data;

        })

    }

    createCompany(formData){

        return axios.post('/api/company',formData).then(response => {

            return response;

        })

    }

    updateCompany(id,formData){

        return axios.put('/api/company/'+id,formData).then(response=>{

            return response;

        });

    }

    deleteCompany(id){

        return axios.delete('/api/company/'+id).then(response=>{

            return response;

        });

    }
}

export default new CompanyDataService;