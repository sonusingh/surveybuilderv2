
class PermissionDataService {
  getAll() {
    return axios.get("/permission");
  }

  get(id) {
    return axios.get(`/permission/${id}`);
  }

  create(data) {
    return axios.post("/permission", data);
  }

  update(id, data) {
    return axios.put(`/permission/${id}`, data);
  }

  delete(id) {
    return axios.delete(`/permission/${id}`);
  }

  deleteAll() {
    return axios.delete(`/permissions`);
  }

  findByTitle(name) {
    return axios.get(`/permission?name=${name}`);
  }
}

export default new PermissionDataService();
