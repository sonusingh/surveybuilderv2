class AdminSurveyDataService{

    getSurvey(page){

        return  axios.get('/api/admin_survey').then(response=>{
           
            return response.data.surveys;

        })

    }
  

}

export default new AdminSurveyDataService;