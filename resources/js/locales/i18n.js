import Vue from 'vue';
import VueI18n from 'vue-i18n';
import enT from 'vuetify/lib/locale/en';
import arT from 'vuetify/lib/locale/ar'

Vue.use(VueI18n);

const messages = {
  ar: {
    ...require('./ar.json'),
    $vuetify :arT,
  },
  en: {
    ...require('./en.json'),
    $vuetify :enT,
  }
}
export default new VueI18n ({
  locale: 'en',
  messages,
  fallbackLocale: 'en',
  silentFallbackWarn: true,
  
})
