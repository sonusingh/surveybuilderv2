/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import router from './routes';
import VueRouter from 'vue-router';
import vuetify from './plugins/vuetify';
import store from './store';
import Index from './Index';
import Landing from './Landing';
import NotifierPlugin from './plugins/notifier'
import i18n from "./locales/i18n";
import LaravelPermissions from 'laravel-permissions';
import {localeMixin} from './utils/locale-mixin';


window.Vue = require('vue');

Vue.use(LaravelPermissions, { persistent: true });
Vue.use(VueRouter);
Vue.use(NotifierPlugin);


window.axios.interceptors.response.use(
    response => {
        return response;
    },
    error => {
        const isLoggedIn=localStorage.getItem("isLoggedIn");
        if ((401 === error.response.status) && (isLoggedIn == 'true')) {
            store.commit('login/setUser',{});
            store.commit('login/setLoggedIn',false);
            localStorage.setItem("isLoggedIn", false);
            location.replace('/');
        }

        return Promise.reject(error);
    }
);
//globalVariables
Vue.prototype.supportedLanguages=JSON.parse(process.env.MIX_LANGUAGES);
const app = new Vue({
    el: '#app',
    mixins: [localeMixin],
    router,
    store,
    vuetify ,
    i18n,
    components: {
        index: Index,
        landing:Landing,
       
    },
    async beforeCreate() {
        this.$store.dispatch("login/loadStoredState");
        this.$store.dispatch("login/loadUser");
       
    },
});
