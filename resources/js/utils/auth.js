export function isLoggedIn() {
    return localStorage.getItem("isLoggedIn") == 'true';
}

export function logIn() {
    localStorage.setItem("isLoggedIn", true);
}

export function logOut() {
    localStorage.setItem("isLoggedIn", false);
    localStorage.setItem("userRole", '');
}

export function isAdmin(){
    return localStorage.getItem("userRole") == 'admin';
}

export function isRole(payload){
    localStorage.setItem("userRole", payload);

}