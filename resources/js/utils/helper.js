import i18n from '../locales/i18n'

export default {
    getText (key) {
        return i18n.t(key)
    }
}