import _ from 'lodash';
function copyDataMixin(){
    
    return {
       
        data(){
            return {
                usersCopy:_.cloneDeep(this.$store.state.user.users.data),
            }
        },
        computed:{
            users(){
                return  _.cloneDeep(this.$store.state.user.users.data);
            }
        },
        watch:{
            users(newValue){
                this.usersCopy=newValue;
            }
        },
    }
}

export default copyDataMixin;
    
