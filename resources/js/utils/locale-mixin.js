import i18n from '../locales/i18n'
import {mapState} from 'vuex'

export var localeMixin = {
  computed: {
    ...mapState(['app_locale','app_dir'])
  },
  watch: {
    app_locale (newValue) {
      this.$i18n.locale = newValue
      i18n.locale = newValue
    },
    app_dir (newValue) {
      this.$vuetify.rtl=newValue
    }
  },
  created: function () {
    this.$i18n.locale = this.app_locale
    this.$vuetify.rtl=this.app_dir
    i18n.locale = this.app_locale
  }
}