import VueRouter from "vue-router";

const routes=[

    {
        path: "/auth/login",

        component: require("./views/auth/LoginComponent").default,

        name: "login",

    },

    {
        path: "/home",

        component: require("./views/HomeComponent").default,

        meta:{ requiresAuth: true ,superAuth:true ,adminAuth: false},

        name: "home"

    },

    {
        path: "/role",

        component: require("./views/RoleComponent").default,

        meta:{ requiresAuth: true, superAuth:true,adminAuth: false },

        name: "role"

    },

    {
      path: "/company",

      component: require("./views/CompanyComponent").default,

      meta:{ requiresAuth: true, superAuth:true,adminAuth: false },

      name: "company"

  },

 

    {
        path: "/permission",

        component: require("./views/PermissionComponent").default,

        meta:{ requiresAuth: true ,superAuth: true ,adminAuth:false },

        name: "permission"

    },

    {
        path: "/app/user",

        component: require("./views/UserComponent").default,

        meta:{ requiresAuth: true ,superAuth: true ,adminAuth: false},

        name: "app_user"

    },

    {

      path:"/survey",

      component: require("./views/SurveyComponent").default,

      meta:{ requiresAuth: true ,superAuth: true ,adminAuth: false},

      name: "survey"

    },
    
    {

      path:"/report",

      component: require("./views/ReportComponent").default,

      meta:{ requiresAuth: true ,superAuth: true,adminAuth:false },

      name: "report"

    },
 
    
    {
      path:"/history",

      component: require("./views/HistoryComponent").default,

      meta:{ requiresAuth: true ,superAuth: true, adminAuth: false },

      name: "history"

    },

    {
      path:"/admin/home",

      component: require("./views/admin/HomeComponent").default,

      meta:{ requiresAuth: true ,adminAuth: true ,superAuth: false},

      name: "admin_home"

    },

    {
      path:"/admin/user",

      component: require("./views/admin/UserComponent").default,

      meta:{ requiresAuth: true ,adminAuth: true ,superAuth: false},

      name: "admin_user"

    },

    {
      path:"/admin/team",

      component: require("./views/admin/TeamComponent").default,

      meta:{ requiresAuth: true ,adminAuth: true ,superAuth: false},

      name: "admin_team"

    },

    {
      path:"/admin/profile",

      component: require("./views/admin/ProfileComponent").default,

      meta:{ requiresAuth: true ,adminAuth: true ,superAuth: false},

      name: "admin_profile"

    },


    {
      path:"/admin/survey",

      component: require("./views/admin/SurveyComponent").default,

      meta:{ requiresAuth: true ,adminAuth: true ,superAuth: false},

      name: "admin_survey"

    },

    {
      path:"/admin/history",

      component: require("./views/admin/HistoryComponent").default,

      meta:{ requiresAuth: true ,adminAuth: true ,superAuth: false},

      name: "admin_history"

    },


]


const router = new VueRouter({

    routes,

    mode:'history'

})

router.beforeEach((to, from, next) => {
  if(to.meta.requiresAuth){
    let arr = localStorage.getItem('userRole');
    if(localStorage.getItem('isLoggedIn')=="false"){
       next({
         path: '/auth/login',
         query: { redirect: to.fullPath }
        })
    }
    else if(to.meta.superAuth){
      if(arr=='super admin'){
        next();
      }
      else{
        next('/admin/home');
      }
    }
    else if(to.meta.adminAuth){
      if(arr=='admin'){
        next();
      }
      else{
        next('/home');
      }
    }
  }else{
    next();
  }

    // if (to.matched.some(record => record.meta.requiresAuth)) {

    //   if (localStorage.getItem('isLoggedIn')=="false") {

    //       next({

    //           path: '/auth/login',

    //           query: { redirect: to.fullPath }

    //       })

    //   } else {

    //     next()

    //   }

    // } else {

    //   next() // make sure to always call next()!

    // }

})

export default router;