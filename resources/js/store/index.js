import Vue from 'vue';
import modules from './modules'
import VuexPersist from 'vuex-persist/dist/umd';


import Vuex from 'vuex';
Vue.use(Vuex);

const vuexPersist = new VuexPersist({
    key: 'app-store',
    storage: window.localStorage,
    reducer: state => ({
      app_locale: state.app_locale,
      app_dir: state.app_dir
    })
  })
const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
    plugins: [vuexPersist.plugin],
    state: {
        app_locale: 'en',
        app_dir : false
    },
    mutations: {
        SET_APP_LOCALE (state, payload) {
            state.app_locale = payload
        },
        SET_APP_DIR (state, payload) {
            state.app_dir = payload
        },
    },
    modules: modules,
    strict: debug,
})