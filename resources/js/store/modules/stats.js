import StatsDataService from '../../services/stats';

const state = {

    total_user:'',

    total_company:'',

    total_survey:''

}

const mutations = {

    SET_TOTAL_USER(state,payload){

      state.total_user = payload;

    },

    SET_TOTAL_SURVEY(state,payload){

        state.total_survey = payload;

    },

    SET_TOTAL_COMPANY(state,payload){

        state.total_company = payload;

    }  
   
}

const actions = {

    async getStats({commit,state}){

        await StatsDataService.getStats().then(response=>{

           commit('SET_TOTAL_COMPANY',response.company);

           commit('SET_TOTAL_USER',response.user);

           commit('SET_TOTAL_SURVEY',response.survey);

        })

    },  

}

export default {

    namespaced:true,

    state,

    mutations,
    
    actions
    
}