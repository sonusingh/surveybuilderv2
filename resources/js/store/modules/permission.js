const state = {
    permissions : [],
    page : 1,
    pageCount : 1,
    itemsPerPage : 5,
    status : ''
}
const mutations = {
    SET_PERMISSION(state,payload){
        state.permissions = payload;
    },
    SET_PAGE(state,payload){
        state.page = payload;
    },
    SET_PAGECOUNT(state,payload){
        state.pageCount = payload;
    },
    SET_ITEMSPERPAGE(state,payload){
        state.itemsPerPage = payload;
    },
    SET_STATUS(state,payload){
        state.status = payload;
    }
   
}

const actions = {
    async getPermissions({commit,state}){
        commit('SET_STATUS',true);
        await axios.get('/api/permission?page='+state.page).then((data)=>{
           commit('SET_PERMISSION',data.data.permissions.data);
           commit('SET_PAGE',data.data.permissions.current_page);
           commit('SET_PAGECOUNT',data.data.permissions.last_page);
           commit('SET_ITEMSPERPAGE',data.data.permissions.per_page);
           commit('SET_STATUS',false);
        })
    },
    async modifyPermission(context,data){
       
        await axios.put('/api/permission/'+data.id,data).then((response)=>{

            context.commit('SET_STATUS',response.data.success);

            context.dispatch('getPermissions');
        });
    },
    async addPermission(context,data){

        await axios.post('/api/permission',data).then((response)=>{
           context.commit('SET_STATUS',response.data.success);
           context.dispatch('getPermissions');

        });
    },
    async destroyPermission(context,id){

        await axios.delete('/api/permission/'+id).then((response)=>{
            context.commit('SET_STATUS',response.data.success);
            context.dispatch('getPermissions');
 
         });
    }
}

export default {
    namespaced:true,
    state,
    mutations,
    actions
}