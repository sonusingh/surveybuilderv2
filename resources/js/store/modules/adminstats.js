import AdminStatsDataService from '../../services/admin_stats';

const state = {

    total_user:'',

    total_survey:''

}

const mutations = {

    SET_TOTAL_USER(state,payload){

      state.total_user = payload;

    },

    SET_TOTAL_SURVEY(state,payload){

        state.total_survey = payload;

    },

   
   
}

const actions = {

    async getStats({commit,state}){

        await AdminStatsDataService.getStats().then(response=>{

           commit('SET_TOTAL_USER',response.user);

           commit('SET_TOTAL_SURVEY',response.survey);

        })

    },  

}

export default {

    namespaced:true,

    state,

    mutations,
    
    actions
    
}