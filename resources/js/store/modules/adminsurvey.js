import AdminSurveyDataService from '../../services/admin_survey';

const state = {

    surveys : [],

    status : ''
}

const mutations = {

    SET_SURVEY(state,payload){

      state.surveys = payload;

    },

   

    SET_STATUS(state,payload){

        state.status = payload;
    }
   
}
const actions = {
    async getSurveys({commit,state}){

        commit('SET_STATUS',true);

        await AdminSurveyDataService.getSurvey().then(response=>{
          
           commit('SET_SURVEY',response);

           commit('SET_STATUS',false);

        })
    },
   
 
  
}

export default {

    namespaced:true,

    state,

    mutations,
    
    actions
}