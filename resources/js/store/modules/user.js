const state = {
    users : {},
    status : ''
}
const mutations = {
    SET_USER(state,payload){
      state.users = payload;
    },
    SET_PAGE(state,payload){
        state.users.current_page = payload;
    },
   
    SET_STATUS(state,payload){
        state.status = payload;
    },
    
   
}


const actions = {
    async getUsers({commit,state},pageNumber){
        commit('SET_STATUS',true);
        await axios.get('/api/app_user?page='+ pageNumber).then((data)=>{
           commit('SET_USER',data.data.users);
           commit('SET_STATUS',false);
        })
    },
    async modifyUser(context,data){
       
        await axios.put('/api/app_user/'+data.id,data).then((response)=>{

            context.commit('SET_STATUS',response.data.success);

            context.dispatch('getUsers');
        });
    },
    async addUser(context,data){

        await axios.post('/api/app_user',data).then((response)=>{
           context.commit('SET_STATUS',response.data.success);
           context.dispatch('getUsers');

        });
    },
    async destroyUser(context,id){

        await axios.delete('/api/app_user/'+id).then((response)=>{
            context.commit('SET_STATUS',response.data.success);
            context.dispatch('getUsers');
 
         });
    }
}

export default {
    namespaced:true,
    state,
    mutations,
    actions
}