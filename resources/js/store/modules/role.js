const state = {
    roles : [],
    page : 1,
    pageCount : 1,
    itemsPerPage : 5,
    status : ''
}
const mutations = {
    SET_ROLE(state,payload){
      state.roles = payload;
    },
    SET_PAGE(state,payload){
        state.page = payload;
    },
    SET_PAGECOUNT(state,payload){
        state.pageCount = payload;
    },
    SET_ITEMSPERPAGE(state,payload){
        state.itemsPerPage = payload;
    },
    SET_STATUS(state,payload){
        state.status = payload;
    }
   
}
const actions = {
    async getRoles({commit,state}){
        commit('SET_STATUS',true);
        await axios.get('/api/role?page='+state.page).then((data)=>{
           commit('SET_ROLE',data.data.roles.data);
           commit('SET_PAGE',data.data.roles.current_page);
           commit('SET_PAGECOUNT',data.data.roles.last_page);
           commit('SET_ITEMSPERPAGE',data.data.roles.per_page);
           commit('SET_STATUS',false);
        })
    },
    async modifyRole(context,data){
       
        await axios.put('/api/role/'+data.id,data).then((response)=>{

            context.commit('SET_STATUS',response.data.success);

            context.dispatch('getRoles');
        });
    },
    async addRole(context,data){

        await axios.post('/api/role',data).then((response)=>{
           context.commit('SET_STATUS',response.data.success);
           context.dispatch('getRoles');

        });
    },
    async destroyRole(context,id){

        await axios.delete('/api/role/'+id).then((response)=>{
            context.commit('SET_STATUS',response.data.success);
            context.dispatch('getRoles');
 
         });
    }
}

export default {
    namespaced:true,
    state,
    mutations,
    actions
}