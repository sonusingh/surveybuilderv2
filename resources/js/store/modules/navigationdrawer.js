
  const state = {
    barColor: 'rgba(0, 0, 0, .8), rgba(0, 0, 0, .8)',
    barImage: '/images/sidebar-1.jpg',
    drawer: null,
    mini:false,
  }

 const mutations = {
    SET_BAR_IMAGE (state, payload) {
      state.barImage = payload
    },
    SET_DRAWER (state, payload) {
      state.drawer = payload
    },
    SET_MINI(state, payload){
      state.mini = payload
    }
  }
  export default {
    namespaced: true,
    state,
    mutations
  }


 