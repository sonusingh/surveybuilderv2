import { isLoggedIn, logOut } from "./../../utils/auth";
    const state = {
       isLoggedIn:false,
       user:{}
    }
    
    const getters = {
        isLoggedIn(state){
            return state.isLoggedIn;
        },
        user(state){
            return state.user;
        },
       
    }
    
    const actions = {
        async loadUser({ commit, dispatch }) {
            if (isLoggedIn()) {
                try {
                   
                   await axios.get("/user").then((data)=>{
                        commit("setUser", data.data.user);
                        commit("setLoggedIn", true);
                       
                    });
                 
                    
                } catch (error) {
                   
                    dispatch("logout");
                }
            }
        },

        logout({ commit }) {
            commit("setUser", {});
            commit("setLoggedIn", false);
            logOut();
        },
        loadStoredState(context) {
          
            context.commit("setLoggedIn", isLoggedIn());
        },
       
    }
   const mutations ={
        setUser(state, payload) {
         
            state.user = payload;
        },
        setRole(state,payload){
           
            Vue.set(state, 'role', payload);
           
        },
        setLoggedIn(state, payload) {
            state.isLoggedIn = payload;
        },
      
    }
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
  }