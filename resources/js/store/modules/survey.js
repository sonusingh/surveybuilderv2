import SurveyDataService from '../../services/survey';

const state = {

    surveys : [],

    page : 1,

    pageCount : 1,

    itemsPerPage : 5,

    status : ''
}

const mutations = {

    SET_SURVEY(state,payload){

      state.surveys = payload;

    },

    SET_PAGE(state,payload){

        state.page = payload;

    },

    SET_PAGECOUNT(state,payload){

        state.pageCount = payload;

    },

    SET_ITEMSPERPAGE(state,payload){

        state.itemsPerPage = payload;

    },

    SET_STATUS(state,payload){

        state.status = payload;
    }
   
}
const actions = {
    async getSurveys({commit,state}){

        commit('SET_STATUS',true);

        await SurveyDataService.getSurvey(state.page).then(response=>{
           
           commit('SET_SURVEY',response.data);

           commit('SET_PAGE',response.current_page);

           commit('SET_PAGECOUNT',response.last_page);
           
           commit('SET_ITEMSPERPAGE',response.per_page);

           commit('SET_STATUS',false);

        })
    },
    async modifySurvey(context,data){
       
        await SurveyDataService.updateSurvey(data.id,data).then(response=>{

            context.commit('SET_STATUS',response.success);

            context.dispatch('getSurveys');
        });
    },
    async addSurvey(context,data){

        await SurveyDataService.createSurvey(data).then(response=>{

           context.commit('SET_STATUS',response.success);

           context.dispatch('getSurveys');

        });
    },
    async destroySurvey(context,id){

        await SurveyDataService.deleteSurvey(id).then(response=>{

            context.commit('SET_STATUS',response.data.success);

            context.dispatch('getSurveys');
 
         });
    }
}

export default {

    namespaced:true,

    state,

    mutations,
    
    actions
}