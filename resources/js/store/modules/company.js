import CompanyDataService from '../../services/company';

const state = {

    companies : {},

    status : ''
}

const mutations = {

    SET_COMPANY(state,payload){

      state.companies = payload;

    },

    SET_PAGE(state,payload){

        state.page = payload;

    },

   

    SET_STATUS(state,payload){

        state.status = payload;

    }
   
}

const actions = {

    async getCompanies({commit,state}){

        commit('SET_STATUS',true);

        await CompanyDataService.getCompany(state.page).then(response=>{

           commit('SET_COMPANY',response.companies);

           commit('SET_STATUS',false);

        })

    },

    async modifyCompany(context,data){
       
        await CompanyDataService.updateCompany(data.id,data).then(response=>{

            context.commit('SET_STATUS',response);

            context.dispatch('getCompanies');

        });

    },

    async addCompany(context,data){

        await CompanyDataService.createCompany(data).then(response=>{

           context.commit('SET_STATUS',response);

           context.dispatch('getCompanies');

        });

    },

    async destroyCompany(context,id){

        await CompanyDataService.deleteCompany(id).then(response=>{

            context.commit('SET_STATUS',response.data.success);

            context.dispatch('getCompanies');
 
        });

    }

}

export default {

    namespaced:true,

    state,

    mutations,
    
    actions
    
}