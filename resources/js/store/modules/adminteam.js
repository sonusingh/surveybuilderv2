import AdminTeamDataService from '../../services/admin_team';

const state = {

    teams:{},

    status:''

}

const mutations = {
    SET_TEAM(state,payload){
      state.teams = payload;
    },
    SET_PAGE(state,payload){
        state.teams.current_page = payload;
    },
   
    SET_STATUS(state,payload){
        state.status = payload;
    },
    
   
}


const actions = {
    async getTeams({commit,state},pageNumber){
        commit('SET_STATUS',true);
        await axios.get('/api/admin_team?page='+ pageNumber).then((data)=>{
           commit('SET_TEAM',data.data.teams);
           commit('SET_STATUS',false);
        })
    },
    async modifyTeam(context,data){
       
        await axios.put('/api/admin_team/'+data.id,data).then((response)=>{

            context.commit('SET_STATUS',response.data.success);

            context.dispatch('getTeams');
        });
    },
    async addTeam(context,data){

        await axios.post('/api/admin_team',data).then((response)=>{
           context.commit('SET_STATUS',response.data.success);
           context.dispatch('getTeams');

        });
    },
    async destroyTeam(context,id){

        await axios.delete('/api/admin_team/'+id).then((response)=>{
            context.commit('SET_STATUS',response.data.success);
            context.dispatch('getTeams');
 
         });
    }
}

export default {
    namespaced:true,
    state,
    mutations,
    actions
}