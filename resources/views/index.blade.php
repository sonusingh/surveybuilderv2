<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Survey Builder</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        
       <!-- <link rel="stylesheet" href="{{ asset('css/app.css') }}"> -->
        <link href="{{ mix('css/app.css') }}" rel="stylesheet">
        
       
    </head>
    <body>
        <div id="app">
           @guest
                <landing></landing>
            @endguest
            @auth()
                <index></index>
            @endauth
        </div>
        
     
        <script src="{{ mix('js/app.js') }}"></script>
     
    </body>
</html>
