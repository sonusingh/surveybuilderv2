<?php

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {

    return $request->user();

});

Route::post('/sanctum_token','API\Mobile\LoginController@login');

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {

    return $request->user()->with(['roles','permissions']);

});

Route::group(['middleware' => ['auth:sanctum']], function () {
    //app user
    Route::post('change_password','API\Mobile\ProfileController@changePassword');
    Route::get('get_survey','API\Mobile\SurveyHandleController@getSurvey');
    //resource
    Route::apiResources([

        'app_user'=>'API\UserController',

        'role'=>'API\RoleController',

        'permission'=>'API\PermissionController',

        'survey'=>'API\SurveyController',

        'company' => 'API\CompanyController',

        'admin_user'=> 'API\Admin\AdminUserController',

        'admin_survey'=> 'API\Admin\AdminSurveyController',

        'admin_team' => 'API\Admin\TeamController'
        
    ]);
   

    Route::get('permissions', 'API\PermissionController')->name('permissions');

    Route::get('roles', 'API\RoleController')->name('roles');

    Route::post('upload','API\UploadController@upload');

    Route::get('get_stats','API\StatsController@getStats');

    Route::get('get_survey_stats','API\StatsController@getSurveys');

    Route::post('question','API\QuestionController@store');

    Route::post('question_response','API\QuestionResponseController@store');

    Route::get('all_roles','API\RoleController@getAll');

    Route::get('all_permissions','API\PermissionController@getAll');

    Route::get('all_survey','API\SurveyController@getAll');

    Route::get('all_companies','API\CompanyController@getAll');

    Route::post('add_survey/{id}','API\SurveyController@saveSurvey');
    
    //Admin
    Route::get('admin_get_stats','API\Admin\StatsController@getStats');

    Route::get('admin_get_survey','API\Admin\StatsController@getSurvey');

    Route::get('admin/all_users','API\Admin\AdminUserController@getAll');

    Route::get('admin/profile','API\Admin\ProfileController@getProfile');

    Route::post('admin/save_profile','API\Admin\ProfileController@updateProfile');
    
    Route::get('admin/get_team','API\Admin\TeamController@getTeam');

    Route::post('admin/save_team_survey/{id}','API\Admin\TeamController@saveTeamSurvey');
    
});