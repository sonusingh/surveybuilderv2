<?php

use Illuminate\Support\Facades\Route;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/',function(){

    return view('index');

})->middleware('guest');

Route::get('/login',function(){

    return redirect('/');

});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    $user=\App\User::where('id',$request->user()->id)->with('roles')->first();
    return response()->json(['user'=>$user],200);

});

Route::get('/home',function(){

    return view('index');

})->name('home');

Route::get('/{any?}', function () {
    
    return view('index');

})->where('any', '^(?!api\/)[\/\w\.-]*');

