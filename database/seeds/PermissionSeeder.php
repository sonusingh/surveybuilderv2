<?php
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionSeeder extends Seeder
{
    /**
     * Create the initial roles and permissions.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();



        // create roles and assign existing permissions
        $super_admin = Role::create(['name' => 'super admin']);
        // Permission::create(['delete Permission']);
        // $super_admin->givePermissionTo('delete permission');
        $super_admin->givePermissionTo(Permission::createResource('category', 'sub_category','user','survey','role','question','option','response','sub_category_option'));
       

        $admin = Role::create(['name' => 'admin']);
       
       
        $app_user= Role::create(['name' => 'user']);
       
        $user = Factory(App\User::class)->create([
            'name' => 'SuperAdmin',
            'email' => 'super@example.com',
            // factory default password is 'password'
        ]);
        $user->assignRole($super_admin);
    }
}