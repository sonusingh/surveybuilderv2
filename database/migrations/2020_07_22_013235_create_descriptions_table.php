<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDescriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('descriptions', function (Blueprint $table) {
            $table->id();
            $table->string('town')->nullable();
            $table->string('road')->nullable();
            $table->string('type')->nullable();
            $table->string('google_locationg')->nullable();
            $table->string('points_of_interest1')->nullable();
            $table->string('points_of_interest2')->nullable();
            $table->string('points_of_interest3')->nullable();
            $table->longText('building_specification')->nullable();
            $table->string('rent')->nullable();
            $table->string('total_shop_rent')->nullable();
            $table->string('photo_0')->nullable();
            $table->string('photo_30')->nullable();
            $table->string('photo_inside')->nullable();
            $table->string('photo_180')->nullable();
            $table->string('submission_id');
            $table->foreignId('survey_id')->constrained('surveys');
            $table->string('date_of_submission')->nullable();
            $table->foreignId('team_user_id')->contrained('team_users');
            $table->string('final_score');
            $table->json('other_details')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('descriptions');
    }
}
