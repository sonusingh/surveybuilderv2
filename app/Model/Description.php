<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Description extends Model

{
    protected $table="descriptions";
    protected $guarded = [];
    protected $fillabel=[
        'town' ,'road' ,'type' , 'google_location' ,'points_of_interest1','points_of_interest2','points_of_interest3','building_specification',
        'rent' , 'total_shop_rent','photo_0','photo_30','photo_inside','photo_180','submission_id','date_of_submission','other_details','survey_id','final_score'
    ];
}