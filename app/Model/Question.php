<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Question extends Model

{
    protected $table="questions";

    protected $fillable = [

       'category_id', 'question','weightage','options','survey_id'

    ];


    public function category(){

        return $this->belongsTo('App\Category');

    }
    
}
