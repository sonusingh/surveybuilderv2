<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SurveyUser extends Model

{
    protected $fillable = [
        'user_id','survey_id','user_type'
    ];
    
    public function user()
    {
        return $this->morphTo();
    }

    // public function user(){
    //     return $this->belongsTo('App\User','user_id');
    // }

    public function survey(){
        return $this->belongsTo('App\Model\Survey','survey_id');
    }

    public function team(){
        return $this->belongsToMany('App\Model\Team','user_id');
    }
    
}
