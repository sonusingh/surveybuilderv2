<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Team extends Model

{
    protected $fillable = [
        'name','company_id'
    ];

    public function user(){

        return $this->belongsToMany('App\User','team_users');

    }

    public function survey(){
        return $this->belongsToMany('App\Model\Survey','team_surveys');
    }


    
}
