<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Company extends Model

{
    //protected $table="companies";

    protected $fillable = [

        'name',  'address', 'phone_number', 'logo_path', 'sector', 'branch'

    ];

    public function user(){

        return $this->belongsToMany('App\User','company_users');
        
    }

    public function survey(){

        return $this->belongsToMany('App\Model\Survey','company_surveys')->withPivot('count');
    }

    public function description(){

        
    }
    
}
