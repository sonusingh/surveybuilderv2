<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class QuestionResponse extends Model

{
    protected $table="question_responses";

    protected $fillable = [

        'score', 'final_score', 'question_id','description_id'

    ];

    public function survey(){

        return $this->belongsTo('App\Survey');

    }

    public function question(){

        return $this->belongsTo('App\Question');
        
    }

}
