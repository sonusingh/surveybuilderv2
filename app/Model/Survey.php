<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model

{
    protected $fillable = [

        'name', 'unique_uuid', 'languages','description','important_dates','timer'

    ];

    

    public function category(){

        return $this->hasMany('App\Model\Category');
    }

    public function question(){

        return $this->hasMany('App\Model\Question')->orderBy('id','DESC');
    }

    public function user(){
        return $this->hasMany('App\User','survey_users');
    }

    public function team(){
        return $this->belongsToMany('App\Model\Team','team_surveys');
    }

    protected static function boot()
    {
        parent::boot();    
    
        static::deleted(function($block)
        {
            $block->category()->delete();
        });
    }    
    
}
