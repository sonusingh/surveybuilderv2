<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model

{
    protected $table="categories";

    protected $fillable = [

        'name','survey_id'

    ];

    public function survey(){

        return $this->belongsTo('App\Model\Survey');
        
    }

    public function question(){

        return $this->hasMany('App\Question');
    }

    
}
