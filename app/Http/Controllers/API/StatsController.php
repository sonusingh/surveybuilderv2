<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Model\Company;
use App\Model\Survey;

class StatsController extends Controller
{
    public function getStats(){
        $user=User::all()->count();
        $survey=Survey::all()->count();
        $company=Company::all()->count();
        return response()->json(['success'=>true,'user'=>$user,'company'=>$company,'survey'=>$survey],200);

    }

    public function getSurveys(){
        $companies =Company::with('survey')->get();
        return response()->json(['success'=>true,'companies'=>$companies],200);
    }
}
