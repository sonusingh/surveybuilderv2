<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()
    {
        $permissions = Permission::paginate(10);

        return response()->json(['permissions'=>$permissions], 200);
    }
    
    /**
     * Create permissions .
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $request->validate([

            'name'=>'required'

        ]);

        $request->request->add(['guard_name'=>'web']);

        Permission::create($request->all());

        return response()->json(["success"=>false], 200);
    }

    /**
     * update permissions .
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Permission $permission)
    {
        $request->validate([

            'name'=>'required'

        ]);

        $request->request->add(['guard_name'=>'web']);

        $permission->update($request->all());

        return response()->json(["success"=>true], 200);
    }

    /**
     * destroy permissions .
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Permission $permission)
    {
        $permission->delete();
        
        return response()->json(["success"=>true], 200);
    }

    /**
     * get all listing of permissions .
     *
     * @return \Illuminate\Http\JsonResponse
     */
    
    public function getAll()
    {
        $permissions=Permission::all();

        return response()->json(['permissions'=>$permissions], 200);
    }

    /**
     * Display a listing of permissions from current logged user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke()
    {
        return auth()->user()->getAllPermissions()->pluck('name');
    }
}
