<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Model\Survey;

use App\Model\Category;
use App\Model\SurveyUser;
use Illuminate\Support\Facades\DB;

class SurveyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()
    {
        $surveys = Survey::with(['category','question'])->paginate(10);

        return response()->json(['surveys'=>$surveys], 200);
    }

    /**
     * create surveys .
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [

            'name' => 'required|unique:surveys,name',

            'unique_uuid' => 'required',

            'timer'      => 'required',

            'description'  => 'required',

            'categories'  => 'required'
            
        ]);

        $survey = Survey::create([

            'name' => $request->input('name'),

            'unique_uuid' =>$request->unique_uuid,

            'timer'  => $request->timer,

            'description' => $request->description

        ]);

        $categories=array();

        foreach($request->categories as $category){

            $data = new Category();

            $data->name = $category['name'];

            array_push($categories,$data);

        }

        $survey->category()->saveMany($categories);
            
        return response()->json(["success"=>true], 200);
    }

    /**
     * update surveys .
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Survey $survey)
    {
        $this->validate($request, [

            'name' => 'required',
            
        ]);
     
        $survey->name = $request->input('name');

        $survey->unique_uuid = $request->unique_uuid;

        $survey->timer = $request->timer;

        $survey->description = $request->description;
      
        $survey->save();

        Category::where('survey_id',$survey->id)->delete();

        $categories=array();
       
        foreach($request->categories as $category){

            $data = new Category();

            $data->name = $category['name'];

            array_push($categories,$data);

        }

        $survey->category()->saveMany($categories);
       
        return response()->json(["success"=>true], 200);
    }

    /**
     * destroy surveys .
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Survey $survey)
    {
        DB::table('company_surveys')->where('survey_id',$survey->id)->delete();
        $survey->category()->delete();

        $survey->delete();
        
        return response()->json(["success"=>true], 200);
    }

    public function getAll(){
        $survey = Survey::all();
        return response()->json(['survey'=>$survey],200);
    }

    public function saveSurvey(Request $request,$id){
        $user = User::where('id',$id)->first();
        foreach($request->all() as $data){
            SurveyUser::firstOrCreate([
                'user_id'=>$user->id,
                'user_type'=>'App\User',
                'survey_id'=>$data['id']
            ]);
        }
        return response()->json(['success'=>true],200);
    }
}
