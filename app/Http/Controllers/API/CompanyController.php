<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Model\Company;
use Illuminate\Support\Facades\DB;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()
    {
        $companies = Company::with('survey')->paginate(10);

        return response()->json(['companies'=>$companies], 200);
    }

    /**
     * create Companies .
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [

            'name' => 'required|unique:companies,name',

            'phone_number' => 'required',

            'address' => 'required',

            'survey' => 'required'

         ]);
        
        $company = Company::create([

            'name' => $request->input('name'),

            'phone_number' => $request->input('phone_number'),

            'sector' => $request->input('sector'),

            'branch' => $request->input('branch'),

            'address' => $request->input('address'),

       //     'logo_path' => $request->input('logo_path')

        ]);
        $company_survey = array();
        foreach ($request->survey as $survey) {
   
           //collect all inserted record IDs
           $company_survey[$survey['id']] = ['count' => $survey['count']];  
       
       }
      
        $company->survey()->sync($company_survey);
        return response()->json(["success"=>true], 200);
    }

    /**
     * update Companies .
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Company $companies)
    {
        $this->validate($request, [

            'phone_number' => 'required',

            'address' => 'required',

        ]);
     
        $companies->phone_number = $request->input('phone_number');

        $companies->sector = $request->input('sector');

        $companies->branch = $request->input('branch');

        $companies->address = $request->input('address');

     //   $companies->logo_path = $request->input('logo_path');
    
     $company_survey = array();
        foreach ($request->survey as $survey) {
   
           //collect all inserted record IDs
           $company_survey[$survey['id']] = ['count' => $survey['count']];  
       
       }
   
       
        $companies->survey()->sync($company_survey,false);
       
        return response()->json(["success"=>true], 200);
    }

    /**
     * destroy Companies .
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Company $companies)
    {
        //return $companies;
        DB::table('company_surveys')->where('company_id',$companies->id)->delete();
       // $companies->survey()->delete();
        $companies->delete();
        
        return response()->json(["success"=>true], 200);
    }

    public function getAll(){
        $companies = Company::all();
        return response()->json(['companies'=>$companies->pluck('name')->flatten()],200);
    }
}
