<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Model\Question;

use App\Model\Survey;
use Exception;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()
    {
        $questions = Question::with('option')->paginate(10);

        return response()->json(['questions'=>$questions], 200);
    }

    /**
     * create Questions .
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        Question::where('survey_id',$request->survey['id'])->delete();
       try {
           foreach ($request->questions as  $question) {
               Question::create([
               'question'=>$question['question'],
               'survey_id'=>$question['survey_id'],
               'weightage'=>$question['weightage'],
                'options'=>json_encode($question['options']),
               'category_id'=>$question['category_id']


               ]);
           }
           return response()->json(["success"=>true], 200);
        }catch(Exception $e){
            return response()->json(["success"=>false], 500);
       }
    }

    /**
     * update Questions .
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Question $question)
    {
        Question::where('survey_id',$request->survey_id)->delete();
        try {
            foreach ($request->questions as  $question) {
                Question::create([
                'question'=>$question['question'],
                'survey_id'=>$question['survey_id'],
                'weightage'=>$question['weightage'],
                 'options'=>json_encode($question['options']),
                'category_id'=>$question['category_id']
 
 
                ]);
            }
            return response()->json(["success"=>true], 200);
         }catch(Exception $e){
             return response()->json(["success"=>false], 500);
        }
    }

    /**
     * destroy Questions .
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Question $question)
    {
        $question->delete();
        
        return response()->json(["success"=>true], 200);
    }
}
