<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()
    {
        $roles = Role::with('permissions')->paginate(10);

        return response()->json(['roles'=>$roles], 200);
    }

    /**
     * create roles .
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [

            'name' => 'required|unique:roles,name',

            'permissions' => 'required',

        ]);

        $role = Role::create(['name' => $request->input('name'),'guard_name'=>'web']);

        $role->syncPermissions($request->input('permissions.*.name'));

        return response()->json(["success"=>true], 200);
    }

    /**
     * update roles .
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Role $role)
    {
        $this->validate($request, [

            'name' => 'required',

            'permissions' => 'required',

        ]);
     
        $role->name = $request->input('name');

        $role->guard_name ='web';

        $role->save();
       
        $role->syncPermissions($request->input('permissions.*.name'));

        return response()->json(["success"=>true], 200);
    }

    /**
     * destroy roles .
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Role $role)
    {
        $role->delete();
        
        return response()->json(["success"=>true], 200);
    }
    /**
     * get all listing of roles .
     *
     * @return \Illuminate\Http\JsonResponse
     */
    
    public function getAll()
    {
        $roles=Role::all();

        return response()->json(['roles'=>$roles], 200);
    }
    /**
     *
     * Display a listing of roles from current logged user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke()
    {
        return auth()->user()->getRoleNames();
    }
}
