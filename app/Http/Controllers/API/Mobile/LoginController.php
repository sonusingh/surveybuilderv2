<?php

namespace App\Http\Controllers\API\Mobile;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        
        $request->validate([
            'email'=>'required|email',
            'password'=>'required',
            'device_name'=>'required'
        ]);
        
        $user = User::where('email', $request->email)->first();
       
        if (!$user || !Hash::check($request->password, $user->password)) {
            
            return response()->json(["success"=>true,"error"=>"Invalid Email Address or Password"],200);
        }
      
        return $user->createToken($request->device_name)->plainTextToken;
    }
}
