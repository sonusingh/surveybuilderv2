<?php

namespace App\Http\Controllers\API\Mobile;

use App\User;
use App\Model\Survey;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;


class SurveyHandleController extends Controller
{
    public function getSurvey(){
        try {
            $data = User::where('id', auth()->user()->id)->with('team.survey.question')->first();
            
            return response()->json(['success'=>true,'surveys'=>$data->team->pluck('survey')],200);
          
        }catch(Exception $e){
            return response()->json(['success'=>false,'message'=>'No Data'],500);
        }
            

    }
}