<?php

namespace App\Http\Controllers\API\Mobile;

use App\User;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;


class ProfileController extends Controller
{
    public function changePassword(Request $request)
    {
        try {
            $user = User::where('id', auth()->user()->id)->first();
            $user->password = Hash::make($request->password);
            $user->save();
            return response()->json(['success=>true'],200);
        }catch(Exception $e){
            return response()->json(['success=>false'],422);
        }
    }

    public function updateProfile(){
        
    }

}