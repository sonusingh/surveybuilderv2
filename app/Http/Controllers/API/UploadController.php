<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Illuminate\Http\File;

use Illuminate\Support\Facades\Storage;

use Illuminate\Http\Request;

class UploadController extends Controller
{
    public function upload(Request $request)
    {
        $path=[];
       
        if ($request->hasFile('files')) {
            foreach ($request->files as $file) {
                $path[]=Storage::putFile('Forms/'.$request->form_id, new File($file));
            }
        }

        return response()->json(['path'=>$path], 200);
    }
}
