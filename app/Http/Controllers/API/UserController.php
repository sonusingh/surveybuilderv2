<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\User;

use DB;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }
    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()
    {
        
        $users = User::with('roles')->paginate(10);

        return response()->json(['users'=>$users], 200);
    }
    
    /**
    * create users .
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function store(Request $request)
    {
        $request->validate([

            'name'=>'required',

            'roles' => 'required',

            'mobile_number' => 'required'

        ]);
        $request->merge(['password'=>bcrypt($request->password)]);
        $user = User::create($request->all());

        $user->assignRole($request->input('roles.*.name'));

        return response()->json(["success"=>true], 200);
    }

    /**
     * update users .
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, User $user)
    {
        $request->validate([

            'name'=>'required'
        ]);
        $request->except('company_name');
        $user->update($request->all());

        DB::table('model_has_roles')->where('model_id', $user->id)->delete();

        $user->assignRole($request->input('roles.*.name'));

        return response()->json(["success"=>true], 200);
    }

    /**
     * destroy users .
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(User $user)
    {
        DB::table('company_users')->where('user_id',$user->id)->delete();
        DB::table('team_users')->where('user_id',$user->id)->delete();
        $user->delete();
        
        return response()->json(["success"=>true], 200);
    }
}
