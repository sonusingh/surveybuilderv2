<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Model\Company;
use App\User;

use DB;

class AdminUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }
    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()
    {
     
       
        $users = User::where('company_name',auth()->user()->company_name,'id')->where('id','!=',auth()->user()->id)->paginate(10);

        return response()->json(['users'=>$users], 200);
    }
    
    /**
    * create users .
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function store(Request $request)
    {
        $request->validate([

            'name'=>'required',

            'mobile_number' => 'required'

        ]);
        $request->merge(['password'=>bcrypt($request->password)]);
        $request->request->add(['company_name'=>auth()->user()->company_name]);
        $user = User::create($request->all());
        $company = Company::where('name',auth()->user()->company_name)->first();
        $company->user()->sync([$user->id]);
        return response()->json(["success"=>true], 200);
    }

    /**
     * update users .
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $user = User::where('id',$id)->first();
        $request->validate([

            'name'=>'required'
        ]);
        $request->merge(['password'=>bcrypt($request->password)]);
        $user->update($request->all());
        return response()->json(["success"=>true], 200);
    }

    /**
     * destroy users .
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($user)
    {
        DB::table('company_users')->where('user_id',$user)->delete();
        DB::table('team_users')->where('user_id',$user)->delete();
       
        $user = User::where('id',$user)->first();
        $user->delete();
        
        return response()->json(["success"=>true], 200);
    }

    public function getAll(){
        $user=User::where('company_name',auth()->user()->company_name)->get();

        return response()->json(['users'=>$user,'success'=>true],200);
    }
}
