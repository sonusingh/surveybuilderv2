<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Model\Team;
use App\Model\Company;
use App\Model\SurveyUser;

use DB;

class TeamController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }
    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()
    {
        $company = Company::where('name',auth()->user()->company_name)->first();
        $teams = Team::where('company_id',$company->id)->with('user')->paginate(10);

        return response()->json(['teams'=>$teams], 200);
    }
    
    /**
    * create Teams .
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function store(Request $request)
    {
        $request->validate([

            'name'=>'required',

            'users' => 'required',


        ]);
        $company = Company::where('name',auth()->user()->company_name)->first();
        $request->request->add(['company_id'=>$company->id]);
        $team = Team::create($request->all());
        $sync_data=[];
        foreach (collect($request->users)->pluck('id') as $item) {

            //collect all inserted record IDs
            $sync_data[$item] = ['company_id' => $company->id];  
        
        }
        $team->user()->sync($sync_data);
        return response()->json(["success"=>true], 200);
    }

    /**
     * update Teams .
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Team $team)
    {
        $request->validate([

            'name'=>'required'
        ]);
        $company = Company::where('name',auth()->user()->company_name)->first();
        $request->request->add(['company_id'=>$company->id]);
        $team->update($request->all());

       

        return response()->json(["success"=>true], 200);
    }

    /**
     * destroy Teams .
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($team)
    {
        DB::table('team_surveys')->where('team_id',$team)->delete();
        DB::table('team_users')->where('team_id',$team)->delete();
        Team::where('id',$team)->delete();
        
        
        return response()->json(["success"=>true], 200);
    }

    public function getTeam(){
      $company = Company::where('name',auth()->user()->company_name)->first();
      $teams = Team::where('company_id',$company->id)->get();
      return response()->json(['teams'=>$teams],200);
    }

    public function saveTeamSurvey(Request $request,$id){
        foreach($request->all() as $data){
            $team = Team::where('id',$data['id'])->first();
            $team->survey()->sync([$id]);
        }
        
        return response()->json(['success'=>true],200);
    }
}
