<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;


use App\User;
use App\Model\Company;

class AdminSurveyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()
    {
        $user =Company::where('name',auth()->user()->company_name)->with(['survey.category','survey.team'])->first();

        return response()->json(['surveys'=>$user->survey->flatten()], 200);
    }


  
}
