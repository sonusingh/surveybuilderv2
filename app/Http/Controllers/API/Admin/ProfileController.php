<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Model\Company;

use App\User;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }

    public function getProfile(){
        $user= auth()->user();
        $company = Company::where('user_id',$user->id)->first();
        return response()->json(['company'=>$company,'user'=>$user],200);
    }

    public function updateProfile(Request $request){
        $user = auth()->user();
        $requestUser = $request->user;
        unset($requestUser['created_at']);
        unset($requestUser['updated_at']);
        $requestCompany = $request->company;
        unset($requestCompany['created_at']);
        unset($requestCompany['updated_at']);
        User::where('id',$user->id)->update($requestUser);
        Company::where('user_id',$user->id)->update($requestCompany);
        return response()->json(['success'=>true],200);

    }
  

}
