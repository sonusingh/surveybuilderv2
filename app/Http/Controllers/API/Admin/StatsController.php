<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Company;

class StatsController extends Controller
{
    public function getStats(){
        $company=Company::where('name',auth()->user()->company_name)->with(['user','survey'])->first();
      
        return response()->json(['success'=>true,'user'=>$company->user->count(),'survey'=>$company->survey->count()],200);

    }

    public function getSurvey(){
        $company=Company::where('name',auth()->user()->company_name)->with('survey')->first();
        return response()->json(['success'=>true,'surveys'=>$company->survey],200);
    }
}
