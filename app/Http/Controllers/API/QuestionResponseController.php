<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Model\Description;

use App\User;
use Image;
use App\Model\QuestionResponse;
use Exception;

class QuestionResponseController extends Controller
{
    public function store(Request $request){
     
        try {
            $user = User::where('id', auth()->user()->id)->with('team')->first();
            $team_user_id = $user->team[0]->pivot->id;
            $description =  $request[0]['description'];
            $description['team_user_id']=$team_user_id;
            if(isset($description['photo_0'])){
               $description['photo_0'] = $this->handlingUpload($description['photo_0']);
            }
            if(isset($description['photo_30'])){
                $description['photo_30'] = $this->handlingUpload($description['photo_30']);
            }
            if(isset($description['photo_180'])){
                $description['photo_180'] = $this->handlingUpload($description['photo_180']);
             }
             if(isset($description['photo_inside'])){
                 $description['photo_inside'] = $this->handlingUpload($description['photo_inside']);
             }
   
          
            $description_t = Description::create($description);
            
            foreach ($request[0]['responses'] as $response) {
                $data = $response;
                $data['description_id']=$description_t->id;
                QuestionResponse::create($data);
            }
    
            return response()->json(["success"=>true], 200);
        }catch(Exception $e){
            return response()->json(["success"=>false,"message"=>$e], 422);
        }
        
    }

    public  function handlingUpload($img){
        

        $name = time().'.' . explode('/', explode(':', substr($img, 0, strpos($img, ';')))[1])[1];

        Image::make($img)->save(public_path('images/').$name);
    
   
          return $name;
    
    }
}
